package hcl.reversenumber;

import java.util.Scanner;

public class ReverseFinder {
	public static int findReverse(int number) {
		int reverse=0;
		while(number>0) {
			int r = number % 10;
			reverse = reverse * 10 +r;
			number = number/10;
		}
		return reverse;
	}

	public static void main(String[] args) {
		Scanner in = new Scanner(System.in);
	int number = in.nextInt();
	System.out.println(findReverse(number));
	}

}
