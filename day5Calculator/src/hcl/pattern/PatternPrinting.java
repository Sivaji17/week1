package hcl.pattern;

import java.util.Scanner;

public class PatternPrinting {
	public static void printPattern(int num) {
		int a=1;
		for(int i=0;i<num;i++) {
			for(int j=0;j<=i;j++) {
				System.out.print(a+" ");
				a++;
			}
			System.out.println(" ");
		}
	}

	public static void main(String[] args) {
		Scanner in = new Scanner(System.in);
		
		int num = in.nextInt();
		printPattern(num);
	}

}
