package com.hcl.interf;
interface Common{
	abstract String markAttendance();
	abstract String dailyTask();
	abstract String displayDetails();	
}
class Employee implements Common{
	public String markAttendance() {
		return "Attendance marked for today";
	}
	public String dailyTask() {
		return " complete coding of module1";
	}
	public String displayDetails() {
		return "employee details";
	}
}
class Manager implements Common{
	public String markAttendance() {
		return " Attendance marked for today";
	}
	public String dailyTasks() {
		return "Create project architecture";
	}
	public String displayDetails() {
		return "Manager details";
	}
	@Override
	public String dailyTask() {
		// TODO Auto-generated method stub
		return null;
	}
}

public class Main {

	public static void main(String[] args) {
		Common i1 = new Employee();
		Common i2 = new Manager();
		System.out.println(i1.markAttendance());
		System.out.println(i1.dailyTask());
		System.out.println(i1.displayDetails());
		System.out.println(i2.markAttendance());
		System.out.println(i2.dailyTask());
		System.out.println(i2.displayDetails());
	}

}
