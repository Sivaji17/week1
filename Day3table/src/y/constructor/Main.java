package y.constructor;
class ThreeDimensionShape{
	double width, height,depth;
	ThreeDimensionShape (double w, double h, double d){
		this.width=w;
		this.height=h;
		this.depth=d;
	}
	ThreeDimensionShape (double l){
		this.width=1;
		this.height=1;
		this.depth=1;
	}
	ThreeDimensionShape() {
		this.width=0;
		this.height=0;
		this.depth=0;
	}
	 double volume(){
		 return width * height * depth;
	 }
}
public class Main {

	public static void main(String[] args) {
		ThreeDimensionShape shape1 = new ThreeDimensionShape(5, 6, 7); 
		ThreeDimensionShape shape2 = new ThreeDimensionShape(); 
		ThreeDimensionShape shape3 = new ThreeDimensionShape(8);
		double volume;
		
		volume = shape1.volume();
		System.out.println(" Volume of shape1 is " + volume);
		
		volume = shape2.volume();
		System.out.println(" Volume of shape2 is " + volume); 
		
		volume = shape3.volume(); 
		System.out.println(" Volume of shape3 is " + volume); 
	}

}
