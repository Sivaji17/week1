package ToManager;

import java.util.Scanner;

public class DeleteUpdateSearch {

	public static void main(String[] args) {
		Scanner s = new Scanner(System.in);
		String[] tasks = new String[5];
		int i,j;
		System.out.println("enter tasks");
		for(i=0;i<5;i++) {
			tasks[i]=s.nextLine();
		}
		System.out.println("enter task to delete");
		String deleted_task = s.nextLine();
		for(i=0;i<5;i++) {
			if(tasks[i].compareTo(deleted_task)==0) {
				for(j=i+1;j<5;j++) {
					tasks[j-1]=tasks[j];
				}
				tasks[j-1]=" ";
				break;
			}	
		}
		for(i=0;i<5;i++)
			System.out.println("task" + (i+1) + " " + tasks[i]);
		int flag=0;
		System.out.println("enter task to search");
		String searched_task = s.nextLine();
		for(i=0;i<5;i++) {
			if(tasks[i].compareTo(searched_task)==0) {
				flag++;
			}
		}
		if(flag>0) {
			System.out.println("element found");
		}
		else {
			System.out.println("element not found");
		}
		System.out.println("enter task to update");
		String updated_task = s.nextLine();
		System.out.println("enter the index to update");
		int index_to_be_updated = s.nextInt();
		tasks[index_to_be_updated-1]=updated_task;
		
		for(i=0;i<5;i++)
			System.out.println("task" + (i+1) + " " + tasks[i]);
		s.close();
	}
}

