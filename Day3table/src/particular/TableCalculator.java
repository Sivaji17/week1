package particular;

import java.util.*;

class TableCalculator {
	static void printTable(int n) {
		int i=1;
		while(i<=10) {
			System.out.println(n*i);
			i=i+1;
		}
	}

	public static void main(String[] args) {
		Scanner in = new Scanner(System.in);
	System.out.println("input");
	
		int n = in.nextInt();
		
		printTable(n);
	
	}

}
